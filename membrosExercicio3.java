package exercicio3;

public class membrosExercicio3{

	private String Nome;
	private int Idade;
	private String Segmento;
	private String TempoEquipe;
	
	public membrosExercicio3(String nome, int idade, String segmento, String tempoEquipe) {
		this.Nome = nome;
		this.Idade = idade;
		this.Segmento = segmento;
		this.TempoEquipe = tempoEquipe;
	}
	
	public membrosExercicio3(String nome, int idade, String segmento) {
		this(nome, idade , segmento, "1 ano");
	}
	
	public membrosExercicio3(String nome, int idade) {
		this(nome, idade, "SEK", "1 mes");
		
	}
	
	public void confirmar() {
		System.out.println("O nome do membro � " + this.Nome);
		System.out.println("A idade do membro �  " + this.Idade);
		System.out.println("O segmento � a " + this.Segmento);
		System.out.println("O tempo na equipe � de " + this.TempoEquipe);
	}
	
	
}

